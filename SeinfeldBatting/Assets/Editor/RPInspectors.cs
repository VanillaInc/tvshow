﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using UnityEditor;

[CustomEditor(typeof(SetRP))]
public class SetGUI : Editor
{
    List<string> relationships;
    readonly int _choiceIndex;
    readonly string[] types = new[] { "Add", "Subtract", "Set" };
    readonly int _typeIndex;
    readonly int _changeBy;

    StringBuilder builder = new StringBuilder();

    public override void OnInspectorGUI()
    {
    #if SHOWINFO
        DrawDefaultInspector();
    #endif

        var data = target as SetRP;
        relationships = new List<string>();

        builder.Clear();

        // Populate string list from relationship list
        foreach (RP r in RPDataMan.relationships)
        {
            relationships.Add(r.name);
        }

        // Create string array from string list
        string[] relationshipStrings = relationships.ToArray();

        GUILayout.Label("--- Choose the relationship you wish to affect ---");

        // Create dropdown from string array
        data.relationshipIndex = EditorGUILayout.Popup("Relationship: ", data.relationshipIndex, relationshipStrings);

        GUILayout.Label("");

        GUILayout.Label("--- Choose how you would like to affect it ---");

        data.amountToChangeBy= EditorGUILayout.IntField("Amount to affect by: ", data.amountToChangeBy);

        data.typeIndex = EditorGUILayout.Popup("Type of change: ", data.typeIndex, types);

        // Update the selected choice in the underlying object
        //data.relationshipIndex = _choiceIndex;
        //data.amountToChangeBy = _changeBy;
        //data.typeIndex = _typeIndex;

            builder.Append(types[data.typeIndex]).Append(" ").Append(data.amountToChangeBy).Append(" ").Append(relationshipStrings[data.relationshipIndex]);

        data.gameObject.name = builder.ToString();

        // Save the changes back to the object
        EditorUtility.SetDirty(target);
    }
}

[CustomEditor(typeof(CheckRP))]
public class CheckGUI : Editor
{
    readonly string[] types = new[] { "<", ">", "=" };

    //int typeIndex = 0;
    //readonly int relationshipIndex = 0;
    //int num = 0;
    public Object scene;

    StringBuilder builder = new StringBuilder();

    public override void OnInspectorGUI()
    {
        //DrawDefaultInspector();

        List<string> relationshipStrings = new List<string>();
        var data = target as CheckRP;

        builder.Clear();

        GUILayout.Label("Check:", EditorStyles.boldLabel);

        // Draw checks >>>>>>>>>>>>>>>>>>

        foreach (RP rp in RPDataMan.relationships)
        {
            relationshipStrings.Add(rp.name);
        }

        GUILayout.BeginHorizontal();

        GUILayout.Label("If ");

        data.relationshipIndex = EditorGUILayout.Popup(data.relationshipIndex, relationshipStrings.ToArray());

        GUILayout.Label("\'s relationship is");

        data.typeInt = EditorGUILayout.Popup(data.typeInt, types);

        data.num = EditorGUILayout.IntField(data.num);

        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();

        GUILayout.Label("Then go to ");

        data.scene = (SceneFlow)EditorGUILayout.ObjectField(data.scene, typeof(SceneFlow), true);

        GUILayout.EndHorizontal();

            builder.Append("If ").Append(relationshipStrings[data.relationshipIndex]).Append(" is ").Append(types[data.typeInt]).Append(" " + data.num);
        
        if(data.scene != null && GUI.changed)
            builder.Append(" go to ").Append(data.scene.name);

        data.gameObject.name = builder.ToString();

        //data.relationshipIndex = relationshipIndex;
        //data.typeInt = typeIndex;
        //data.num = num;
        //data.scene = (SceneFlow)scene;

        EditorUtility.SetDirty(target);
    }
}