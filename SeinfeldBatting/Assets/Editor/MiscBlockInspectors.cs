﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using System.Text;

[CustomEditor(typeof(SetSpriteTo))]
public class SpriteChangeInspector : Editor
{
    StringBuilder builder = new StringBuilder();
    public bool UI;

    public override void OnInspectorGUI()
    {
        //DrawDefaultInspector();

        var data = target as SetSpriteTo;

        builder.Clear();

        GUILayout.Space(10);

        GUILayout.BeginHorizontal();

        if(UI)
            GUILayout.Label("Change Image", EditorStyles.boldLabel);
        else
            GUILayout.Label("Change Sprite", EditorStyles.boldLabel);

        UI = GUILayout.Toggle(UI, "UI");

        GUILayout.EndHorizontal();

        GUILayout.Space(2);

        if(UI)
        {
            GUILayout.BeginHorizontal();

            GUILayout.Label("Change ");

            data.image = EditorGUILayout.ObjectField(data.image, typeof(Image), true) as Image;

            GUILayout.Label("\'s image to ");

            data.sprite = EditorGUILayout.ObjectField(data.sprite, typeof(Sprite), true) as Sprite;

            GUILayout.EndHorizontal();
        }
        else
        {
            GUILayout.BeginHorizontal();

            GUILayout.Label("Change ");

            data.subject = EditorGUILayout.ObjectField(data.subject, typeof(SpriteRenderer), true) as SpriteRenderer;

            GUILayout.Label("\'s sprite to ");

            data.sprite = EditorGUILayout.ObjectField(data.sprite, typeof(Sprite), true) as Sprite;

            GUILayout.EndHorizontal();
        }

        if (GUI.changed && data.subject != null && data.sprite != null)
            data.gameObject.name = builder.Append(data.subject.name).Append(" spte = ").Append(data.sprite.name).ToString();
        else if(GUI.changed && data.subject != null && data.image != null)
            data.gameObject.name = builder.Append(data.image.name).Append(" img = ").Append(data.sprite.name).ToString();

        EditorUtility.SetDirty(target);
    }
}

[CustomEditor(typeof(PlayAnimation))]
public class PlayAnimationInspector : Editor
{
    StringBuilder builder = new StringBuilder();

    public override void OnInspectorGUI()
    {
        var data = target as PlayAnimation;

        builder.Clear();

        GUILayout.Space(10);

        GUILayout.BeginHorizontal();

        data.anim = EditorGUILayout.ObjectField(data.anim, typeof(Animator), true) as Animator;

        GUILayout.Label(".SetTrigger(");

        data.animString = EditorGUILayout.TextField(data.animString);

        GUILayout.Label(")");

        GUILayout.EndHorizontal();

        GUILayout.Space(10);

        data.showReset = GUILayout.Toggle(data.showReset, "Reset Trigger");

        if (data.showReset)
        {
            GUILayout.BeginHorizontal();

            GUILayout.Label("     ");

            data.resetString = EditorGUILayout.TextField("ResetTrigger(", data.resetString);

            GUILayout.Label(")");

            GUILayout.EndHorizontal();
        }
        else
        {
            data.resetString = "";
        }

        if (GUI.changed)
            data.gameObject.name = builder.Append("Set ").Append(data.anim.gameObject.name).Append(" Trigger: ").Append(data.animString).ToString();

        EditorUtility.SetDirty(target);
    }
}

[CustomEditor(typeof(Dialogue))]
public class DialogueInspec : Editor
{
    StringBuilder builder = new StringBuilder();

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        builder.Clear();

        var data = target as Dialogue;

        if (GUI.changed)
            data.gameObject.name = builder.Append(RPDataMan.relationships[data.dGBlocks[0].RPIndex].name).ToString();

        EditorUtility.SetDirty(target);
    }
}

[CustomEditor(typeof(Pause))]
public class PauseInspec : Editor
{
    StringBuilder builder = new StringBuilder();

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        builder.Clear();

        var data = target as Pause;

        if (GUI.changed)
            data.gameObject.name = builder.Append("Pause for: ").Append(data.time).Append(" second(s)").ToString();

        EditorUtility.SetDirty(target);
    }
}

[CustomEditor(typeof(MoveTo))]
public class MoveToInspector : Editor
{
    StringBuilder builder = new StringBuilder();

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        builder.Clear();

        var data = target as MoveTo;

        if(data.cc != null)
            builder.Append("Move ").Append(data.cc.name);

        if(data.destination)
            builder.Append(" to ").Append(data.destination.name);

        data.name = builder.ToString();

        EditorUtility.SetDirty(target);
    }
}