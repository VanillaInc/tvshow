﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(DGBlock))]
public class DialogueInspector : PropertyDrawer
{
    List<string> names = new List<string>();
    GUIContent lbl;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        lbl = new GUIContent("Dialogue: ", "Insert dialogue, retard");

        EditorGUI.BeginProperty(position, label, property);
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), lbl);

        var indent = EditorGUI.indentLevel;

        EditorGUI.indentLevel = 0;

        // Calculate rects
        var profileRect = new Rect(position.x, position.y, 50, position.height - 40);
        var aliasRect = new Rect(position.x + 35, position.y, position.width - 35, position.height - 40);
        var dialogueRect = new Rect(position.x, position.y + 5, position.width, 50);

        // Draw fields
        EditorGUI.PropertyField(profileRect, property.FindPropertyRelative("profile"), GUIContent.none);
        EditorGUI.PropertyField(dialogueRect, property.FindPropertyRelative("dialogue"), GUIContent.none);

        EditorGUI.indentLevel = indent;

        if(names.Count == 0)
        {
            foreach (RP relation in RPDataMan.relationships)
            {
                names.Add(relation.name);
            }
        }

        if (GUI.changed)
        {
            property.FindPropertyRelative("RPIndex").serializedObject.ApplyModifiedProperties();
        }

        property.FindPropertyRelative("RPIndex").intValue = EditorGUI.Popup(aliasRect, property.FindPropertyRelative("RPIndex").intValue, names.ToArray());

        EditorGUI.EndProperty();
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return base.GetPropertyHeight(property, label) + 40;
    }
}

[CustomEditor(typeof(RPDataMan))]
public class RPDataInspec : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        var data = target as RPDataMan;

        if(RPDataMan.relationships != data.relations)
            RPDataMan.relationships = data.relations;

        EditorUtility.SetDirty(target);
    }
}