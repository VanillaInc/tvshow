﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartBattleFromScene : MonoBehaviour
{
    public GameObject obj, obj2;

    public EnemySpawner es;

    private void OnEnable()
    {
        es.StartGame();

        Destroy(obj2);
        Destroy(obj);
    }
}
