﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChoiceButton : MonoBehaviour
{
    public GameObject directory;

    public void Awake()
    {
        GetComponent<UnityEngine.UI.Button>().onClick.AddListener(delegate { GoToPlaceInScene(); });
    }

    void GoToPlaceInScene()
    {
        directory.GetComponent<SceneFlow>().PlayScene();
        FindObjectOfType<DialogueManager>().NextDialogue();
        FindObjectOfType<DialogueManager>().DestroyChoices();
    }
}
