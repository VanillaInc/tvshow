﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveSet : MonoBehaviour
{
    public GameObject gameObj;
    public bool active;

    private void OnEnable()
    {
        gameObj.SetActive(active);
        transform.parent.GetComponent<SceneFlow>().NextEvent();
        gameObject.SetActive(false);
    }
}
