﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Choices : MonoBehaviour
{
    public Choice[] choices;

    private void OnEnable()
    {
        Animator canvas = GameObject.Find("Canvas").GetComponent<Animator>();

        if(canvas != null)
        {
            canvas.SetTrigger("Choices");
        }

        FindObjectOfType<DialogueManager>().CreateChoices(choices, this);
    }

    public void Finish()
    {
        Animator canvas = GameObject.Find("Canvas").GetComponent<Animator>();
        transform.parent.GetComponent<SceneFlow>().EndScene();

        if (canvas != null)
        {
            canvas.SetTrigger("NoChoice");
        }

        gameObject.SetActive(false);
    }
}
