﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DIntro : MonoBehaviour
{
    public bool dBox;
    public new string name;
    public bool pfp;
    public Sprite image;

    private void OnEnable()
    {
        if(pfp)
            DialogueManager.instance.pfp.gameObject.SetActive(true);

        if(dBox)
            DialogueManager.instance.dialogueBox.gameObject.SetActive(true);

        if (image != null)
            DialogueManager.instance.pfp.sprite = image;

        DialogueManager.instance.NmOut.text = name;

        transform.parent.GetComponent<SceneFlow>().NextEvent();
        gameObject.SetActive(false);
    }
}
