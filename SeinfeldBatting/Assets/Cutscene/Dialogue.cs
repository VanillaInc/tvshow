﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dialogue : MonoBehaviour
{
    public DGBlock[] dGBlocks;
	
	[HideInInspector]
	public List<string> names;

    private DialogueManager dm;
    private SceneFlow director;

    public void SetNames(string[] newNames)
    {
        names.Clear();
        names.AddRange(names);
    }

    private void OnEnable()
    {
        director = transform.parent.GetComponent<SceneFlow>();
        dm = FindObjectOfType<DialogueManager>();

        dm.TriggerDialogue(this);
    }

    public void Finished()
    {
        director.NextEvent();
        gameObject.SetActive(false);
    }
}