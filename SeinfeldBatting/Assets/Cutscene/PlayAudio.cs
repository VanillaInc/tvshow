﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAudio : MonoBehaviour
{
    public AudioSource audioSource;

    private void OnEnable()
    {
        if(audioSource != null)
        {
            audioSource.Play();

            while (audioSource.isPlaying)
            {
                if (!audioSource.isPlaying)
                {
                    break;
                }
            }

        }

        transform.parent.GetComponent<SceneFlow>().NextEvent();
        gameObject.SetActive(false);
    }
}
