﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class CheckRP : MonoBehaviour
{
    public int typeInt;
    public int num;
    public SceneFlow scene;
    public int relationshipIndex;

    private SceneFlow director;

    private void OnEnable()
    {
        director = transform.parent.GetComponent<SceneFlow>();

        switch (typeInt)
        {
            case 0:
                LessThan();
                break;
            case 1:
                MoreThan();
                break;
            case 2:
                EqualsTo();
                break;
        }
    }

    void LessThan()
    {
        if(RPDataMan.relationships[relationshipIndex].rp < num)
        {
            scene.PlayScene();
            director.EndScene();
            gameObject.SetActive(false);
        }
        else
        {
            director.NextEvent();
            gameObject.SetActive(false);
        }
    }

    void MoreThan()
    {
        if (RPDataMan.relationships[relationshipIndex].rp > num)
        {
            scene.PlayScene();
            director.EndScene();
            gameObject.SetActive(false);
        }
        else
        {
            director.NextEvent();
            gameObject.SetActive(false);
        }
    }

    void EqualsTo()
    {
        if (RPDataMan.relationships[relationshipIndex].rp == num)
        {
            scene.PlayScene();
            director.EndScene();
            gameObject.SetActive(false);
        }
        else
        {
            director.NextEvent();
            gameObject.SetActive(false);
        }
    }
}