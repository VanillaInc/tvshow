﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetCameraAfterZoom : MonoBehaviour
{
    private void OnEnable()
    {
        FindObjectOfType<CutsceneCamera>().ResetSize();

        transform.parent.GetComponent<SceneFlow>().NextEvent();
        gameObject.SetActive(false);
    }
}
