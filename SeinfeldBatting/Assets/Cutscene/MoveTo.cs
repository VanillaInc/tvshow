﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTo : MonoBehaviour
{
    [Header("Subject")]
    public CutsceneController cc;
    
    [Header("Properties")]
    public Transform destination;
    public float moveSpeed;
    public bool run;

    private void OnEnable()
    {
        cc.moveSpeed = moveSpeed;

        cc.MoveTo(destination, this);
    }

    public void Finish()
    {
        transform.parent.GetComponent<SceneFlow>().NextEvent();

        gameObject.SetActive(false);
    }
}
