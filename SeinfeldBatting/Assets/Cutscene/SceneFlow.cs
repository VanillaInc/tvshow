﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneFlow : MonoBehaviour
{
    public List<GameObject> events;
    public int eventIndex;
    Reference r;

    string origName;

    private void Start()
    {
        events = new List<GameObject>();
    }

    [ContextMenu("Start Scene Debug (May fuck shit up)")]
    public void PlayScene()
    {
        r = Reference.instance;
        r.activeScene = this;
        r.inScene = true;

        origName = gameObject.name;
        gameObject.name = gameObject.name + " (Active)";

        eventIndex = 0;
        events.Clear();

        foreach(Transform child in transform)
        {
            events.Insert(0, child.gameObject);
        }

        /*
        for (int i = 0; i < transform.childCount; i++)
        {
            events.Insert(0, transform.GetChild(i).gameObject);
        }
        */

        events.Reverse();
        
        events[0].SetActive(true);

    }

    public void NextEvent()
    {
        events.RemoveAt(0);

        if (events.Count > 0)
            events[0].SetActive(true);
        else
        {
            EndScene();
            return;
        }
            

        eventIndex = events[0].transform.GetSiblingIndex();
    }

    public void EndScene()
    {
        //foreach(Transform g in transform)
        //{
        //    g.gameObject.SetActive(false);
        //}

        gameObject.name = origName;

        r.inScene = false;

        FindObjectOfType<DialogueManager>().ClearTextIn();

        // THIS MOTHERFUCKLER OH MY GOD I CAN"T FUC K ING BELIEVE THAT
        //events.Clear();
        // TH IS WAS IT DO NOT ENABLE THJIS HOLY SHIT

        if (Reference.instance.activeScene.Equals(this))
            Reference.instance.activeScene = null;
    }
}
