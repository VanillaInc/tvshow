﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndScene : MonoBehaviour
{
    private void OnEnable()
    {
        transform.parent.GetComponent<SceneFlow>().EndScene();
        gameObject.SetActive(false);
    }
}
