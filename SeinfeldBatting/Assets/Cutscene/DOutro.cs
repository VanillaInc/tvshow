﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DOutro : MonoBehaviour
{
    public bool dBox;
    public bool pfp;

    private void OnEnable()
    {
        //Reference.instance.dout = this;

        DialogueManager.instance.ClearTextIn();

        if(dBox)
            DialogueManager.instance.dialogueBox.gameObject.SetActive(false);

        if (pfp)
            DialogueManager.instance.pfp.gameObject.SetActive(false);

        DialogueManager.instance.pfp.sprite = null;

        transform.parent.GetComponent<SceneFlow>().NextEvent();
        gameObject.SetActive(false);
    }
}
