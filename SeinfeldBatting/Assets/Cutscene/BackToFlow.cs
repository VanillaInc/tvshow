﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackToFlow : StateMachineBehaviour
{
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    //override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        SceneFlow sf = Reference.instance.activeScene;

        PlayAnimation pa = sf.transform.GetChild(sf.eventIndex).GetComponent<PlayAnimation>();

        if(pa != null)
        {
            pa.AnimDone();
        }

        //DIntro di = Reference.instance.dint;

        //if (di != null)
        //{
            //di.Finish();
        //}

        //DOutro dout = Reference.instance.dout;

        //if (dout != null)
        //{
        //    dout.Finish();
        //}

    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
