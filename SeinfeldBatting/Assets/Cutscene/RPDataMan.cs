﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class RPDataMan : MonoBehaviour
{
    public static List<RP> relationships;

    public List<RP> relations;

    private void Start()
    {
        relationships = relations;
    }

    public static void AddRP(RP relationship, int rpToAdd)
    {
        int index = relationships.IndexOf(relationship);
        RP temp = relationships[index];

        temp.rp += rpToAdd;

        relationships[index] = temp;
    }

    public static void SubRP(RP relationship, int rpToSub)
    {
        int index = relationships.IndexOf(relationship);
        RP temp = relationships[index];

        temp.rp -= rpToSub;

        relationships[index] = temp;
    }

    public static void SetRP(RP relationship, int rpToSet)
    {
        int index = relationships.IndexOf(relationship);
        RP temp = relationships[index];

        temp.rp = rpToSet;

        relationships[index] = temp;
    }

    public static void AddNewRP(RP relationship)
    {
        relationships.Add(relationship);
    }

    public static void ApplyRP()
    {
        relationships = FindObjectOfType<RPDataMan>().relations;
    }
}

[System.Serializable]
public struct RP
{
    public string name;
    public int rp;
    public Color nameColor;
}