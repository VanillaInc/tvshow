﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadUnityScene : MonoBehaviour
{
    public string sceneName;

    public bool cutsceneBlock;

    private void OnEnable()
    {
        if(cutsceneBlock)
        {
            SceneController.instance.LoadScene(sceneName);
        }
    }
}