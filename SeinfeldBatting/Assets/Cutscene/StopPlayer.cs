﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopPlayer : MonoBehaviour
{
    public bool canMove;

    private void OnEnable()
    {
        ShipController.canMove = canMove;

        transform.parent.GetComponent<SceneFlow>().NextEvent();

        gameObject.SetActive(false);
    }
}
