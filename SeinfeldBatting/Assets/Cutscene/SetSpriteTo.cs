﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetSpriteTo : MonoBehaviour
{
    public SpriteRenderer subject;
    public Image image;
    public Sprite sprite;

    private void OnEnable()
    {
        if(subject != null)
            subject.sprite = sprite;

        if (image != null)
            image.sprite = sprite;

        transform.parent.GetComponent<SceneFlow>().NextEvent();
        gameObject.SetActive(false);
    }
}
