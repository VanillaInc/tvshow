﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogueManager : MonoBehaviour {

    public KeyCode[] next;

    private bool talking;

    public GameObject dialogueBox;
    public TMP_Text DgOut;
    public TMP_Text NmOut;
    public Image pfp;
    public Transform buttonSpawn;
    public GameObject button;

    public Queue<string> sentances;
    public Queue<string> names;
    public Queue<Sprite> pfps;
    public Queue<Color> n_colors;

    private Dialogue sender;
    private Choices sendee;
    private List<GameObject> buttons;

    bool typing = false;
    bool finishedString = false;
    string sent;

    #region Instance

    public static DialogueManager instance;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    #endregion

    private void Start()
    {
        sentances = new Queue<string>();
        names = new Queue<string>();
        pfps = new Queue<Sprite>();
        n_colors = new Queue<Color>();
        buttons = new List<GameObject>();
    }

    private void Update()
    {
        if(Next() && talking)
        {
            if(typing)
            {
                QuickFinishDialogue();
                return;
            }

            if(finishedString)
            {
                NextDialogue();
                finishedString = false;
            }
        }
    }

    public void TriggerDialogue(Dialogue dialogue)
    {
        // Clear Queues
        sentances.Clear();
        names.Clear();
        pfps.Clear();
        n_colors.Clear();

        // Set gloabals/references
        sender = dialogue;
        talking = true;
        typing = false;

        // Gather dialogue data
        foreach(DGBlock dg in dialogue.dGBlocks)
        {
            sentances.Enqueue(dg.dialogue);
            pfps.Enqueue(dg.profile);
            names.Enqueue(RPDataMan.relationships[dg.RPIndex].name);
            n_colors.Enqueue(RPDataMan.relationships[dg.RPIndex].nameColor);
        }

        if(!dialogueBox.gameObject.activeSelf)
        {
            dialogueBox.gameObject.SetActive(true);
            ClearTextIn();
        }

        StopAllCoroutines();

        // Start dialogue loop
        NextDialogue();
    }

    public void NextDialogue()
    {
        typing = false;
        finishedString = false;

        // Check if there is still dialogue
        if (sentances.Count == 0)
        {
            EndDialogue();
            return;
        }

        // Set data
        Sprite sprite = pfps.Dequeue();

        if (sprite != null)
            pfp.sprite = sprite;

        string nameString = names.Dequeue();

        if (nameString != "a")
            NmOut.text = nameString;

        Color nameColor = n_colors.Dequeue();

        NmOut.color = nameColor;

        string sentance = sentances.Dequeue();

        if (sentance != null)
        {
            sent = sentance;
            StopAllCoroutines();
            StartCoroutine(TypeSentace(sentance));
        }
    }

    IEnumerator TypeSentace(string sentance)
    {
        //Type letters out
        DgOut.text = "";

        finishedString = false;
        typing = true;

        foreach (char letter in sentance.ToCharArray())
        {
            DgOut.text += letter;
            yield return null;
        }

        finishedString = true;
        typing = false;
    }

    void QuickFinishDialogue()
    {
        finishedString = true;
        StopAllCoroutines();
        typing = false;
        DgOut.text = sent;
    }

    void EndDialogue()
    {
        talking = false;
        sender.Finished();
    }

    public void CreateChoices(Choice[] choices, Choices requester)
    {
        talking = false;

        foreach(Choice c in choices)
        {
            GameObject choiceP = Instantiate(button, buttonSpawn.position, Quaternion.identity, buttonSpawn);
            choiceP.GetComponent<ChoiceButton>().directory = c.directory;
            choiceP.transform.GetChild(0).GetComponent<TMP_Text>().text = c.text;
            buttons.Add(choiceP);
        }

        sendee = requester;
    }

    public void DestroyChoices()
    {
        foreach(GameObject g in buttons)
        {
            Destroy(g);
        }

        sendee.Finish();
        buttons.Clear();
    }

    public void ClearTextIn()
    {
        DgOut.text = "";
        NmOut.text = "";
        StopAllCoroutines();
        sentances.Clear();
        names.Clear();
        pfps.Clear();
    }

    bool Next()
    {
        foreach(KeyCode k in next)
        {
            if(Input.GetKeyDown(k))
            {
                return true;
            }
        }

        return Input.GetMouseButtonDown(0);
    }
}

[System.Serializable, ExecuteInEditMode]
public struct DGBlock
{
    public Sprite profile;
    public int RPIndex;

    [TextArea(3, 10)]
    public string dialogue;
}

[System.Serializable]
public struct Choice
{
    [TextArea(2, 10)]
    public string text;
    public GameObject directory;
}