﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeNameColor : MonoBehaviour
{
    public Color color;
    private DialogueManager dm;

    private void OnEnable()
    {
        dm = FindObjectOfType<DialogueManager>();

        if(dm != null)
        {
            dm.NmOut.color = color;
        }

        transform.parent.GetComponent<SceneFlow>().NextEvent();
        gameObject.SetActive(false);
    }
}
