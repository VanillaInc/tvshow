﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoomInOn : MonoBehaviour
{
    [Header("Camera")]
    public CutsceneCamera cam;

    [Header("Properties")]
    public Transform focus;
    public float size;
    public float speed;

    private void OnEnable()
    {
        if (cam == null)
        {
            cam = Camera.main.GetComponent<CutsceneCamera>();
        }

        cam.ZoomInOn(focus, size, speed, this);
    }

    public void Finish()
    {
        transform.parent.GetComponent<SceneFlow>().NextEvent();
        gameObject.SetActive(false);
    }
}
