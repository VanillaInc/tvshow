﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClearDialogueBox : MonoBehaviour
{
    private void OnEnable()
    {
        FindObjectOfType<DialogueManager>().DgOut.text = "";
        transform.parent.GetComponent<SceneFlow>().NextEvent();
        gameObject.SetActive(false);
    }
}
