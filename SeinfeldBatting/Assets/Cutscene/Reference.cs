﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reference : MonoBehaviour
{
    public SceneFlow activeScene;
    public DIntro dint;
    public DOutro dout;
    public bool inScene;

    #region Instance

    public static Reference instance;

    private void Start()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    #endregion
}
