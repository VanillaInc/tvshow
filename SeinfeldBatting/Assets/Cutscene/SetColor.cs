﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetColor : MonoBehaviour
{
	public SpriteRenderer subject;
	public Image image;
	public Color color;
	
    void OnEnable()
	{
		if(subject != null)
			subject.color = color;
		
		if(image != null)
			image.color = color;
		
		transform.parent.GetComponent<SceneFlow>().NextEvent();
		gameObject.SetActive(false);
	}
}
