﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAnimation : MonoBehaviour
{
    public Animator anim;
    public string animString;
    public bool showReset;
    public string resetString = "None";

    private void OnEnable()
    {
        if (resetString != "None" || resetString != "")
        {
            anim.ResetTrigger(resetString);
        }

        anim.SetTrigger(animString);
        StartCoroutine(Failsafe());
    }

    IEnumerator Failsafe()
    {
        yield return new WaitForSecondsRealtime(10f);

        Debug.Log("Failsafe trigger, fix your shit", gameObject);

        anim.SetTrigger(animString);
    }

    public void AnimDone()
    {
        StopAllCoroutines();
        transform.parent.GetComponent<SceneFlow>().NextEvent();
        gameObject.SetActive(false);
    }
}
