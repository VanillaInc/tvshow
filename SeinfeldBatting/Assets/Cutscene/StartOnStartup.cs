﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartOnStartup : MonoBehaviour
{
    public ConvoDirector convos;

    private void Update()
    {
        StartScene();
        Destroy(gameObject);
    }

    private void StartScene()
    {
        convos.StartConversation();
    }
}
