﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneStarter : MonoBehaviour
{
    public ConvoDirector convos;
    public bool onMouseDown;

    private void OnMouseDown()
    {
        Reference r = Reference.instance;

        if(onMouseDown && !r.inScene)
        {
            StartScene();
        }
    }

    private void StartScene()
    {
        convos.StartConversation();
    }
}
