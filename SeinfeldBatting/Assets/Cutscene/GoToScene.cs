﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoToScene : MonoBehaviour
{
    public SceneFlow scene;

    private void OnEnable()
    {
        transform.parent.GetComponent<SceneFlow>().EndScene();
        scene.PlayScene();
        gameObject.SetActive(false);
    }
}
