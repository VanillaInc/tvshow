﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour
{
    public float time;

    private void OnEnable()
    {
        StartCoroutine(pause());
    }

    IEnumerator pause()
    {
        yield return new WaitForSeconds(time);
        transform.parent.GetComponent<SceneFlow>().NextEvent();
        gameObject.SetActive(false);
    }
}
