﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class SetRP : MonoBehaviour
{
    //[HideInInspector]
    public int relationshipIndex;

    //[HideInInspector]
    public int typeIndex;

    //[HideInInspector]
    public int amountToChangeBy;

    private void OnEnable()
    {
        switch (typeIndex)
        {
            case 0:
                RPDataMan.AddRP(RPDataMan.relationships[relationshipIndex], amountToChangeBy);
                break;
            case 1:
                RPDataMan.SubRP(RPDataMan.relationships[relationshipIndex], amountToChangeBy);
                break;
            case 2:
                RPDataMan.SetRP(RPDataMan.relationships[relationshipIndex], amountToChangeBy);
                break;
        }

        transform.parent.GetComponent<SceneFlow>().NextEvent();
        gameObject.SetActive(false);
    }
}