﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConvoDirector : MonoBehaviour
{
    public GameObject convoHolder;

    //[HideInInspector]
    public List<SceneFlow> convos;

    private int currentConvo = 0;

    private void Awake()
    {
        convos = new List<SceneFlow>();
    }

    public void StartConversation()
    {
        foreach (Transform child in convoHolder.transform)
        {
            SceneFlow s = child.transform.GetChild(0).GetComponent<SceneFlow>();

            if (s != null)
                convos.Insert(0, s);
        }

        convos.Reverse();

        convos[currentConvo].PlayScene();

        if(currentConvo < convos.Count - 1)
        {
            currentConvo++;
        }
    }

    public void RewindTime()
    {
        currentConvo = 0;
    }
}