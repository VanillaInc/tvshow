﻿Shader "Custom/CurvedOutline" {

	Properties{
		_MainTex("Texture", 2D) = "white" {}
		_Color("Color", Color) = (1, 1, 1, 1)
		_OutlineThick("Thickness", Int) = 1
	}

		SubShader{

		Cull off
		Blend One OneMinusSrcAlpha

		Pass{

		CGPROGRAM

		#pragma vertex vertexFunc
		#pragma fragment fragmentFunc
		#include "UnityCG.cginc"

		sampler2D _MainTex;

	struct v2f {
		float4 pos : SV_POSITION;
		half2 uv : TEXCOORD0;
	};

	v2f vertexFunc(appdata_base v) {
		v2f o;
		o.pos = UnityObjectToClipPos(v.vertex);
		o.uv = v.texcoord;

		return o;
	}

	fixed4 _Color;
	float4 _MainTex_TexelSize;
	uint _OutlineThick;

	fixed4 fragmentFunc(v2f i) : COLOR {

		half4 c = tex2D(_MainTex, i.uv);
		half4 outlineC = _Color;

		fixed upRightAlpha = tex2D(_MainTex, i.uv + (fixed2(0, _MainTex_TexelSize.y) * (_OutlineThick - 1)) + (fixed2(_MainTex_TexelSize.x, 0) * (_OutlineThick - 1))).a;
		fixed downRightAlpha = tex2D(_MainTex, i.uv - (fixed2(0, _MainTex_TexelSize.y) * (_OutlineThick - 1)) + (fixed2(_MainTex_TexelSize.x, 0) * (_OutlineThick - 1))).a;
		fixed upLeftAlpha = tex2D(_MainTex, i.uv + (fixed2(0, _MainTex_TexelSize.y) * (_OutlineThick - 1)) - (fixed2(_MainTex_TexelSize.x, 0) * (_OutlineThick - 1))).a;
		fixed downLeftAlpha = tex2D(_MainTex, i.uv - (fixed2(0, _MainTex_TexelSize.y) * (_OutlineThick - 1)) - (fixed2(_MainTex_TexelSize.x, 0) * (_OutlineThick - 1))).a;

		fixed upRightAlphaZero = tex2D(_MainTex, i.uv + fixed2(0, _MainTex_TexelSize.y) + fixed2(_MainTex_TexelSize.x, 0)).a;
		fixed downRightAlphaZero = tex2D(_MainTex, i.uv - fixed2(0, _MainTex_TexelSize.y) + fixed2(_MainTex_TexelSize.x, 0)).a;
		fixed upLeftAlphaZero = tex2D(_MainTex, i.uv + fixed2(0, _MainTex_TexelSize.y) - fixed2(_MainTex_TexelSize.x, 0)).a;
		fixed downLeftAlphaZero = tex2D(_MainTex, i.uv - fixed2(0, _MainTex_TexelSize.y) - fixed2(_MainTex_TexelSize.x, 0)).a;

		fixed upAlpha = tex2D(_MainTex, i.uv + fixed2(0, _MainTex_TexelSize.y) * _OutlineThick).a;
		fixed downAlpha = tex2D(_MainTex, i.uv - fixed2(0, _MainTex_TexelSize.y) * _OutlineThick).a;
		fixed rightAlpha = tex2D(_MainTex, i.uv + fixed2(_MainTex_TexelSize.x, 0) * _OutlineThick).a;
		fixed leftAlpha = tex2D(_MainTex, i.uv - fixed2(_MainTex_TexelSize.x, 0) * _OutlineThick).a;

		fixed upAlphaOne = tex2D(_MainTex, i.uv + fixed2(0, _MainTex_TexelSize.y)).a;
		fixed downAlphaOne = tex2D(_MainTex, i.uv - fixed2(0, _MainTex_TexelSize.y)).a;
		fixed rightAlphaOne = tex2D(_MainTex, i.uv + fixed2(_MainTex_TexelSize.x, 0)).a;
		fixed leftAlphaOne = tex2D(_MainTex, i.uv - fixed2(_MainTex_TexelSize.x, 0)).a;

		if (c.a == 0)
		{
			if (_OutlineThick == 1)
			{
				if (upAlpha == 1 || downAlpha == 1 || rightAlpha == 1 || leftAlpha == 1
					|| upRightAlpha == 1 || downRightAlpha == 1 || downLeftAlpha == 1 || upLeftAlpha == 1
					|| upRightAlphaZero == 1 || downRightAlphaZero == 1 || downLeftAlphaZero == 1 || upLeftAlphaZero == 1)
				{
					return outlineC;
				}
				else if (upAlpha == 1 || downAlpha == 1 || rightAlpha == 1 || leftAlpha == 1)
				{
					return outlineC;
				}
			}
			else if (_OutlineThick > 1)
			{
				if (upAlpha == 1 || downAlpha == 1 || rightAlpha == 1 || leftAlpha == 1
					|| upAlphaOne == 1 || downAlphaOne == 1 || rightAlphaOne == 1 || leftAlphaOne == 1
					|| upRightAlpha == 1 || downRightAlpha == 1 || downLeftAlpha == 1 || upLeftAlpha == 1)
				{
					return outlineC;
				}
				else if (upAlpha == 1 || downAlpha == 1 || rightAlpha == 1 || leftAlpha == 1
					|| upAlphaOne == 1 || downAlphaOne == 1 || rightAlphaOne == 1 || leftAlphaOne == 1)
				{
					return outlineC;
				}
			}
		}

		uint alpha = downAlphaOne + upAlphaOne + leftAlphaOne + rightAlphaOne;
		uint diagAlpha = upRightAlphaZero + downRightAlphaZero + upLeftAlpha + downLeftAlpha;

		alpha += diagAlpha;

		if (c.a == 1)
		{
			if (alpha / 2 == 1)
			{
				return outlineC;
			}
		}

		c.rgb *= c.a;
		outlineC.a *= ceil(c.a);
		outlineC.rgb *= outlineC.a;

		return c;
	}

		ENDCG
	}
	}
}