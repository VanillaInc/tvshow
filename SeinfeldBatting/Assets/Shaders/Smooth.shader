﻿Shader "Custom/Smooth" {

	Properties {
		_MainTex("Texture", 2D) = "white" {}
		_Color("Color", Color) = (1, 1, 1, 1)
	}

		SubShader {

			Cull off
			Blend One OneMinusSrcAlpha

			Pass {

			CGPROGRAM

			#pragma vertex vertexFunc
			#pragma fragment fragmentFunc
			#include "UnityCG.cginc"

			sampler2D _MainTex;
				
			struct v2f {
				float4 pos : SV_POSITION;
				half2 uv : TEXCOORD0;
			};

			v2f vertexFunc(appdata_base v) {
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = v.texcoord;

				return o;
			}

			fixed4 _Color;
			float4 _MainTex_TexelSize;

			fixed4 fragmentFunc(v2f i) : COLOR {

				half4 c = tex2D(_MainTex, i.uv);
				half4 color = _Color;

				float4 black = float4(0.0f, 0.0f, 0.0f, 1.0f);

				fixed4 upColor = tex2D(_MainTex, i.uv + fixed2(0, _MainTex_TexelSize.y));
				fixed4 downColor = tex2D(_MainTex, i.uv - fixed2(0, _MainTex_TexelSize.y));
				fixed4 rightColor = tex2D(_MainTex, i.uv + fixed2(_MainTex_TexelSize.x, 0));
				fixed4 leftColor = tex2D(_MainTex, i.uv - fixed2(_MainTex_TexelSize.x, 0));

				fixed4 upRightColor = tex2D(_MainTex, i.uv + (fixed2(0, _MainTex_TexelSize.y) + fixed2(_MainTex_TexelSize.x, 0)));
				fixed4 downRightColor = tex2D(_MainTex, i.uv - (fixed2(0, _MainTex_TexelSize.y) + fixed2(_MainTex_TexelSize.x, 0)));
				fixed4 upLeftColor = tex2D(_MainTex, i.uv + (fixed2(0, _MainTex_TexelSize.y)) - (fixed2(_MainTex_TexelSize.x, 0)));
				fixed4 downLeftColor = tex2D(_MainTex, i.uv - (fixed2(0, _MainTex_TexelSize.y)) - (fixed2(_MainTex_TexelSize.x, 0)));

				if (c.a == 1)
				{
					float4 diff = upColor - c;

					if (any(black.rgb + c) && upColor.a == 1)
					{
						c = lerp(c, upColor, 0.5f);
					}

					diff = downColor - c;

					if (any(black.rgb + c) && downColor.a == 1)
					{
						c = lerp(c, downColor, 0.5f);
					}

					diff = leftColor - c;

					if (any(black.rgb + c) && leftColor.a == 1)
					{
						c = lerp(c, leftColor, 0.5f);
					}

					diff = rightColor - c;

					if (any(black.rgb + c) && rightColor.a == 1)
					{
						c = lerp(c, rightColor, 0.5f);
					}

					diff = upRightColor - c;

					if (any(black.rgb + c) && upRightColor.a == 1)
					{
						c = lerp(c, upRightColor, 0.5f);
					}

					diff = downRightColor - c;

					if (any(black.rgb + c) && downRightColor.a == 1)
					{
						c = lerp(c, downRightColor, 0.5f);
					}

					diff = upLeftColor - c;

					if (any(black.rgb + c) && upLeftColor.a == 1)
					{
						c = lerp(c, upLeftColor, 0.5f);
					}

					diff = downRightColor - c;

					if (any(black.rgb + c) && downRightColor.a == 1)
					{
						c = lerp(c, downRightColor, 0.5f);
					}
				}

				c.rgb *= c.a;

				return lerp(c, color, color.a);
			}

			ENDCG
		}
	}
}