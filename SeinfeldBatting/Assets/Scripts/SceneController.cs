﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
	public UnityEngine.UI.Slider slider;
    public TMPro.TMP_Text text;
	
	public GameObject loadscrn;
	
	public static SceneController instance;
	
	private void Start()
	{
		if(instance == null)
		{
			instance = this;
		}
		else
		{
			Destroy(this);
		}
	}
	
    public void LoadScene(string Sname)
    {
        StartCoroutine(LoadAsync(Sname));
		loadscrn.SetActive(true);
    }

    IEnumerator LoadAsync(string sceneName)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneName);

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / 0.9f);

            slider.value = progress;

            text.text = progress * 100 + "%";

            yield return null;
        }
		
		loadscrn.SetActive(false);
    }
}