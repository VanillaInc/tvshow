﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowScript : MonoBehaviour
{
    public PerformanceObject performance;

    public Transform target;
    SpriteRenderer sr;
    bool checkFade;
    int interval;

    private void Start()
    {
        sr = GetComponent<SpriteRenderer>();

        interval = performance.blockInterval;
    }

    public void FixedUpdate()
    {
        if(Time.frameCount % interval == 1)
        {
            Vector2 trn = new Vector2(target.position.x, transform.position.y);

            transform.position = trn;

            if (checkFade)
            {
                sr.color = new Color(sr.color.r, sr.color.g, sr.color.g, 1 - (target.position - transform.position).sqrMagnitude);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        ShipController sc = collision.GetComponent<ShipController>();

        if (sc != null)
        {
            checkFade = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        ShipController sc = collision.GetComponent<ShipController>();

        sr.color = new Color(1, 1, 1, 0);

        if (sc != null)
        {
            checkFade = false;
        }
    }
}
