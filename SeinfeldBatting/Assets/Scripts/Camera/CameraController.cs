﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private Camera cam;
    //private GillController brett;

    public GameObject target;

    public float camSpeed;

    public GameObject currentRoom;

    // Use this for initialization
    void Start()
    {
        cam = GetComponent<Camera>();
        //brett = FindObjectOfType<GillController>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector2 targetPos = Vector2.zero;

        transform.position = Vector3.Lerp(transform.position, 
                                         new Vector3(targetPos.x,
                                                     transform.position.y,
                                                     transform.position.z),
                                         camSpeed * Time.deltaTime);
    }

    public void FocusOn(GameObject focusee)
    {
        if (focusee != null)
        {
            target = focusee;
            //camSpeed = new Vector2(10, 10);
        }
        else
        {
            //target = brett.gameObject;
        }
    }
}
