﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutsceneCamera : MonoBehaviour
{
    float OrigSize;

    Camera cam;
    GameObject came;

    float newSize;
    float newSpeed;

    Transform newFocus;
    Transform newZoom;

    ZoomInOn zio;

    bool zoomingIn;

    private void Start()
    {
        cam = GetComponent<Camera>();

        came = transform.parent.gameObject;

        OrigSize = cam.orthographicSize;
    }

    public void FocusOn(Transform subject, float speed = 5f)
    {
        newFocus = subject;
        newSpeed = speed;
        StartCoroutine(focusIn());
    }

    public void ZoomInOn(Transform subject, float size, float speed = 5f, ZoomInOn z = null)
    {
        newSpeed = speed;
        newSize = size;
        newZoom = subject;
        zio = z;
        zoomingIn = cam.orthographicSize > size;
        StartCoroutine(zoomIn());
    }

    public void ResetSize()
    {
        transform.position = came.transform.position;
        cam.orthographicSize = OrigSize;
    }

    IEnumerator zoomIn()
    {
        while (cam.orthographicSize != newSize)
        {
            cam.transform.position = Vector3.Lerp(transform.position, new Vector3(newZoom.position.x, newZoom.position.y, transform.position.z), newSpeed * Time.deltaTime);
            cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, newSize, newSpeed * Time.deltaTime);

            if (zoomingIn)
            {
                if (cam.orthographicSize < newSize + 0.01)
                    cam.orthographicSize = newSize;
            }
            else
            {
                if (cam.orthographicSize > newSize - 0.01)
                    cam.orthographicSize = newSize;
            }

            yield return null;
        }

        zio.Finish();
    }

    IEnumerator focusIn()
    {
        while(cam.transform.position != newFocus.position)
        {
            cam.transform.position = Vector3.Lerp(transform.position, newFocus.position, newSpeed);

            yield return null;
        }
    }
}
