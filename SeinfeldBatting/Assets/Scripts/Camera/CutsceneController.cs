﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutsceneController : MonoBehaviour
{
    public float moveSpeed;

    private bool moving;
    private bool moveRight;

    private Transform dest;

    private MoveTo moveTo;

    private Animator anim;
    private Rigidbody2D rb;

    void Start()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if(anim != null)
            anim.SetFloat("VelocityX", rb.velocity.x);

        if(moving)
        {
            if (KeepRunningTo(dest, moveRight))
                rb.velocity = new Vector2(moveSpeed * BoolToInt(moveRight), rb.velocity.y);
            else
            {
                moveTo.Finish();
                rb.velocity = Vector2.zero;
                moving = false;
            }
        }
    }

    public void MoveTo(Transform location, MoveTo m)
    {
        moveRight = location.position.x > transform.position.x;
        dest = location;
        moveTo = m;
        moving = true;
    }

    int BoolToInt(bool b)
    {
        if (b)
            return 1;
        else
            return -1;
    }

    bool KeepRunningTo(Transform loc, bool r)
    {
        if(r)
        {
            if (transform.position.x >= loc.position.x)
            {
                return false;
            }
        }
        else
        {
            if(transform.position.x <= loc.position.x)
            {
                return false;
            }
        }

        return true;
    }
}
