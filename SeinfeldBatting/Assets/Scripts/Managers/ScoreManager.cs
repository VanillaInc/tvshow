﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager instance;

    public int totalScore;
    public int displayScore;

    public int amountKilled;

    private void Start()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    public static int Score()
    {
        return instance.displayScore;
    }

    public delegate void changeScore();
    public event changeScore ScoreChanged;

    public static void AddScore(int score)
    {
        instance.totalScore += score;

        instance.StartCoroutine(addScore(score));

        instance.ScoreChanged?.Invoke();
    }

    static IEnumerator addScore(int score)
    {
        while (instance.displayScore < instance.totalScore)
        {
            instance.displayScore += 1;

            yield return null;
        }
    }

    public static void RemoveScore(int score)
    {
        instance.totalScore -= score;

        instance.ScoreChanged?.Invoke();
    }
}
