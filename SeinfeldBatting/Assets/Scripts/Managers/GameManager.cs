﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public PlayerAttributes pa;
    public static GameManager instance;
    private ShipController player;
    private HudManager hud;

    public int lives = 3;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        SceneManager.activeSceneChanged += NewScene;

        player = FindObjectOfType<ShipController>();

        lives = pa.lives;

        player.PlayerDeath += PlayerDeath;
    }

    void NewScene(Scene current, Scene next)
    {
        player = FindObjectOfType<ShipController>();

        player.PlayerDeath += PlayerDeath;
    }

    void PlayerDeath()
    {
        lives -= 1;

        
    }

    private void Update()
    {
        if(player == null)
        {
            player = FindObjectOfType<ShipController>();

            player.PlayerDeath += PlayerDeath;
        }

        if(hud == null)
        {
            hud = FindObjectOfType<HudManager>();
        }

        if(lives == 0)
        {
            OnGameOver();
        }
    }

    public delegate void gameOver();
    public event gameOver GameOver;

    public void OnGameOver()
    {
        GameOver?.Invoke();

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

}
