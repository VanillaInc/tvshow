﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class HudManager : MonoBehaviour
{
    public PlayerAttributes pa;

    private GameManager gm;
    private ShipController sc;

    public Slider health;
    public TMP_Text healthText;
    public List<Image> heads;
    public TMP_Text additionalHeads;
    public TMP_Text score;

    public Score4Life[] scoresToGiveLives;

    [System.Serializable]
    public class Score4Life
    {
        public int score;
        [HideInInspector] public bool given;
    }

    private void Start()
    {
        gm = FindObjectOfType<GameManager>();
        sc = FindObjectOfType<ShipController>();

        sc.PlayerDeath += UpdateHeadCount;
        sc.PlayerGainLife += UpdateHeadCount;

        SceneManager.activeSceneChanged += NewScene;

        ScoreManager.instance.ScoreChanged += CheckScore4Life;
    }

    public void Update()
    {
        score.text = ScoreManager.Score().ToString();

        health.maxValue = pa.maxHealth;
        health.value = sc.health;

        healthText.text = sc.health.ToString();

        if (gm.lives > 3)
        {
            additionalHeads.text = "+" + (gm.lives - 3);
        }

        additionalHeads.gameObject.SetActive(gm.lives > 3);
    }

    void NewScene(Scene current, Scene next)
    {
        UpdateHeadCount();
    }

    public void UpdateHeadCount()
    {
        foreach(Image i in heads)
        {
            if(i != null)
                i.gameObject.SetActive(i.transform.GetSiblingIndex() < gm.lives);
        }
    }

    public void CheckScore4Life()
    {
        foreach(Score4Life s4l in scoresToGiveLives)
        {
            if(!s4l.given && s4l.score < ScoreManager.Score())
            {
                gm.lives += 1;

                s4l.given = true;
            }
        }
    }
}
