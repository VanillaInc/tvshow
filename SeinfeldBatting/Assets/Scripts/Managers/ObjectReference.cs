﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectReference : MonoBehaviour
{
    #region Singleton

    public static ObjectReference instance;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    #endregion

    public Transform chargeArea;
    public Transform[] chargeDestinations;
}
