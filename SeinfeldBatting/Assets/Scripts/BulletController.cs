﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D)), RequireComponent(typeof(BoxCollider2D))]
public class BulletController : MonoBehaviour
{
    private Rigidbody2D rb;

    [HideInInspector] public float damage;
    [HideInInspector] public float speed;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();

        rb.velocity = new Vector2(0, speed);

        Destroy(gameObject, 1.1f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        EnemyHealthController ehc = collision.GetComponent<EnemyHealthController>();

        if(ehc != null)
        {
            if (ehc.TakeDamage(damage) == 0)
            {
                ehc.Die();
            }
        }

        if (!collision.CompareTag("Non Bullet"))
		    Destroy(gameObject);
    }
}
