﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventCutscene : MonoBehaviour, IEvent
{
    public ConvoDirector convo;

    public void FinishedGame()
    {
        convo.StartConversation();
    }
}
