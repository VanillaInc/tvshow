﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class ShipController : MonoBehaviour
{
    public PlayerAttributes data;

    [HideInInspector] public float health;
    private float currentShotTime;

    public float _invincibleTime;
    private float invincibleTime;

    public int frameOfBlank;

    [HideInInspector] public Transform firePoint;
    private Rigidbody2D rb;
    private SpriteRenderer sr;

    public static bool canMove;

    #region Singleton

    private static ShipController instance;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    #endregion

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();

        health = data.maxHealth;
        invincibleTime = _invincibleTime;

        firePoint = transform.GetChild(0);

        DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        float horiz = Input.GetAxisRaw("Horizontal");
        float vert = Input.GetAxisRaw("Vertical");

        if(canMove)
            rb.velocity = new Vector2(data.moveSpeed * horiz, data.moveSpeed * vert);

        Vector2.ClampMagnitude(rb.velocity, data.moveSpeed);

        if (Input.GetKey(data.fire))
        {
            currentShotTime -= Time.deltaTime;

            if(currentShotTime <= 0)
            {
                Shoot();

                currentShotTime = data.fireSpeed;
            }
        }
        else
        {
            currentShotTime = 0;
        }

        if(health <= 0)
        {
            RemoveLife();
        }
    }

    void Shoot()
    {
        BulletController newBullet = Instantiate(data.bullet, firePoint.position, Quaternion.identity) as BulletController;
        newBullet.speed = data.bulletSpeed;
        newBullet.damage = data.bulletDamage;
    }

    bool vuln = true; // Is vulnerable

    public void Hurt(float damage)
    {
        if(vuln)
        {
            health -= damage;
            vuln = false;
            
            StartCoroutine(Damaged());
        }
    }

    IEnumerator Damaged()
    {
        while(invincibleTime > 0)
        {
            invincibleTime -= Time.deltaTime;

            if(Time.frameCount % frameOfBlank == 1)
            {
                sr.color = new Color(1, 1, 1, 0);
            }
            else
            {
                sr.color = new Color(1, 1, 1, 1);
            }

            yield return null;
        }

        invincibleTime = _invincibleTime;

        sr.color = new Color(1, 1, 1, 1);

        vuln = true;
    }

    public delegate void playerDeath();
    public event playerDeath PlayerDeath;

    public void RemoveLife()
    {
        health = data.maxHealth;

        PlayerDeath?.Invoke();
    }

    public delegate void playerLife();
    public event playerLife PlayerGainLife;

    public void GainLife()
    {
        PlayerGainLife?.Invoke();
    }

    public void Respawn()
    {

    }
}