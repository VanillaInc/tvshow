﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Performance Setting", menuName = "Settings/Performance")]
public class PerformanceObject : ScriptableObject
{
    public int blockInterval;
    public int chaserInterval; // No more than 5!
}
