﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Player Attributes", menuName = "Gameplay/Player Attributes")]
public class PlayerAttributes : ScriptableObject
{
    public float maxHealth;

    public int lives;

    public float moveSpeed;

    [Tooltip("In shots per second")]
    public float fireSpeed;

    public KeyCode fire;

    public float bulletSpeed;
    public float bulletDamage;

    public BulletController bullet;
}
