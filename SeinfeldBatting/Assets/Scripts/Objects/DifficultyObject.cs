﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Difficulty", menuName = "Gameplay/Difficulty")]
public class DifficultyObject : ScriptableObject
{
    public new string name;
    public float chaseSpeed;
    public float chaseDamage;
}
