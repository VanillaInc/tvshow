﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolScript : MonoBehaviour
{
    public Transform subject;

    public float moveSpeed;

    public Transform[] destinations;
    private Transform currentDestination;

    private int destinationIndex = 1;

    // Start is called before the first frame update
    void Start()
    {
        destinations = ObjectReference.instance.chargeDestinations;

        currentDestination = destinations[destinationIndex];
    }

    // Update is called once per frame
    void Update()
    {
        if (subject != null)
        {
            subject.position = Vector3.MoveTowards(subject.position, currentDestination.position, moveSpeed * Time.deltaTime);

            if (subject.position == currentDestination.position)
            {
                destinationIndex++;

                if (destinationIndex == destinations.Length)
                {
                    destinationIndex = 0;
                }

                currentDestination = destinations[destinationIndex];
            }
        }
    }
}