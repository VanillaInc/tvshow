﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealthController : MonoBehaviour
{
    public float health;
    private float maxHealth;
    public Collider2D hitbox;
    public bool invincible = false;

    private void Start()
    {
        maxHealth = health;
    }

    private void Update()
    {
        if(health <= 0)
        {
            Die();
        }
    }

    public float TakeDamage(float damage)
    {
        if(!invincible)
        {
            health -= damage;
        }

        return health;
    }

    public delegate void onDie();
    public event onDie OnDeath;

    public void Die()
    {
        OnDeath?.Invoke();

        health = maxHealth;
    }
}
