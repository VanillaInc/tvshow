﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpArea : MonoBehaviour
{
    public Material outline;
    public Material sprite;

    public List<SpriteRenderer> enemies;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Enter");

        PowerUpController puc = collision.GetComponent<PowerUpController>();

        if (puc == null)
        {
            if(!collision.CompareTag("Non Bullet"))
            {
                enemies.Insert(0, collision.GetComponent<SpriteRenderer>());

                enemies[0].material = outline;

                collision.GetComponent<Enemy>().OnEnemyDeath += ResetMaterialOnDeath;

                ApplyProtection(collision);
            }

        }
    }

    void ApplyProtection(Collider2D info)
    {
        info.GetComponent<EnemyHealthController>().invincible = true;
    }

    void ResetMaterialOnDeath(SpriteRenderer enemy)
    {
        enemy.material = sprite;
    }

    private void OnDisable()
    {
        // Get rid of protection

        foreach(SpriteRenderer sr in enemies)
        {
            if(!sr.CompareTag("Non Bullet"))
            {
                sr.material = sprite;
                sr.GetComponent<EnemyHealthController>().invincible = false;
            }
        }
    }
}
