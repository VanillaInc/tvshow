﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class chargeAtPlayer : Enemy, IPooledEnemy
{
    [Header("Class Variables")]
	public Transform target;

    public float moveSpeed;

    private Rigidbody2D rb;

    int interval;

    public override void Start()
    {
        interval = po.chaserInterval;

        base.Start();
    }

    public void OnObjectRePool()
    {
        rb = GetComponent<Rigidbody2D>();

        if (target == null)
        {
            target = FindObjectOfType<ShipController>().transform;
        }
    }

    public void LateUpdate()
	{
        if (Time.frameCount % interval == 1)
            rb.velocity = transform.up * moveSpeed;
        
        if(Time.frameCount % interval == 1)
            transform.up = target.position - transform.position;

        /*
        transform.Translate(Vector3.up * Time.deltaTime * moveSpeed);
        */
    }

    public override void OnPlayerHit()
    {

    }

    public override void Death()
	{
        base.Death();
	}
}
