﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpController : Enemy, IPooledEnemy
{
    // Start is called before the first frame update
    override public void Start()
    {
        base.Start();
    }

    public void OnObjectRePool() // Called from IPooledEnemy when the object is repooled
    {
        
    }
}
