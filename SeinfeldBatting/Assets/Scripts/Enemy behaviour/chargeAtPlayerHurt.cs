﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class chargeAtPlayerHurt : MonoBehaviour
{
    public float damage;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        ShipController sc = collision.GetComponent<ShipController>();

        if(sc != null)
        {
            sc.health -= damage;

            transform.parent.GetComponent<chargeAtPlayer>().Death();
        }
    }
}
