﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToChargeArea : MonoBehaviour, IPooledEnemy
{
    public Transform chargeArea;
    public float moveSpeed = 2f;

    private SpriteRenderer sr;
    private EnemyHealthController ehc;

    public void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
        ehc = GetComponent<EnemyHealthController>();

        ehc.invincible = true;

        sr.color = new Color(0.4481132f, 1f, 0.6119177f);
    }

    public void OnObjectRePool()
    {
        GetComponent<PatrolScript>().enabled = false;
        GetComponent<ChargeEnemy>().enabled = false;

        ehc.invincible = true;

        chargeArea = ObjectReference.instance.chargeArea;

        StartCoroutine(MoveToPos());
    }

    IEnumerator MoveToPos()
    {
        while (transform.position != chargeArea.position)
        {
            transform.position = Vector2.MoveTowards(transform.position, chargeArea.position, moveSpeed * Time.deltaTime);

            yield return null;
        }

        if(GetComponent<ChargeEnemy>() != null)
        {
            GetComponent<PatrolScript>().enabled = true;
            GetComponent<ChargeEnemy>().enabled = true;
            ehc.invincible = false;
        }

        sr.color = new Color(1, 1, 1);
    }

    private void Update()
    {
        if (chargeArea == null)
        {
            chargeArea = ObjectReference.instance.chargeArea;

            StartCoroutine(MoveToPos());
        }
    }
}
