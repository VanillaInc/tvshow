﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargeEnemy : Enemy, IPooledEnemy
{
    [Header("Class Variables")]
    public float _chargeTime;
    private float chargeTime;

    public float _fireTime;
    private float fireTime;

    bool charge = true;

    public GameObject lazer;
    public ParticleSystem chargeEffect;

    public BoxCollider2D lazerHurtBox;

    public void OnObjectRePool()
    {
        GetComponent<PatrolScript>().enabled = false;
        GetComponent<ChargeEnemy>().enabled = false;
        ehc.invincible = true;
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();

        lazer.SetActive(false);
        lazerHurtBox.enabled = false;

        chargeTime = _chargeTime;
        fireTime = _fireTime;
    }

    // Update is called once per frame
    void Update()
    {
        if(charge)
        {
            if(chargeEffect != null && !chargeEffect.isPlaying)
            {
                chargeEffect.Play();
                chargeEffect.gameObject.SetActive(true);
            }

            chargeTime -= Time.deltaTime;

            if(chargeTime <= 0)
            {
                charge = false;

                StartCoroutine(Fire());
            }
        }
    }

    IEnumerator Fire()
    {
        lazer.SetActive(true);
        lazerHurtBox.enabled = true;

        chargeEffect.Stop();
        chargeEffect.gameObject.SetActive(false);

        while(fireTime > 0)
        {
            fireTime -= Time.deltaTime;
            yield return null;
        }

        fireTime = _fireTime;
        chargeTime = _chargeTime;

        charge = true;

        lazer.SetActive(false);
        lazerHurtBox.enabled = false;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        ShipController sc = collision.GetComponent<ShipController>();

        if(sc != null)
        {
            sc.Hurt(damage);
        }
    }
}
