﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public Animator warningGradient;

    public int currentRound;
    public int currentWave;

    [HideInInspector] private EnemyPooler ep;
    public float amountSpawned = 0;
    public int enemiesInRound;

    public bool lastRound;

    public List<GameObject> enemies;

    public EnemyRound[] rounds;

    private void Start()
    {
        ep = EnemyPooler.instance;
    }

    public void StartGame()
    {
        StartCoroutine(StartRound(rounds[currentRound]));
    }

    private void Update()
    {
        if(lastRound)
        {
            if (ScoreManager.instance.amountKilled == enemiesInRound)
            {
                enemiesInRound = -1;

                FinishedKillingWaves();

                ScoreManager.instance.amountKilled = 0;
            }
        }
    }

    IEnumerator StartRound(EnemyRound enemyRound)
    {
        warningGradient.Play("warning");

        int temp = 0;

        foreach(Wave w in enemyRound.waves)
        {
            temp += w.amountToSpawn;
        }

        enemiesInRound = temp;

        lastRound = false;

        yield return new WaitForSeconds(3f);

        yield return StartCoroutine(SpawnWave(enemyRound.waves[currentWave]));
    }

    IEnumerator SpawnWave(Wave wave)
    {
        if(currentWave == rounds[currentRound].waves.Length - 1)
        {
            lastRound = true;
        }

        while (amountSpawned < wave.amountToSpawn)
        {
            GameObject enemy = ep.SpawnFromPool(wave.enemyTag, wave.spawnPoint.position, Quaternion.identity);
            enemy.GetComponent<Enemy>().father = this;
            enemy.GetComponent<Enemy>().OnEnemyDeath += CheckEnemyCount;
            enemies.Add(enemy);

            amountSpawned++;

            yield return null;
        }

        FinishedSpawningWave();
    }

    void FinishedSpawningWave()
    {
        if (currentWave < rounds[currentRound].waves.Length - 1)
        {
            currentWave++;
            amountSpawned = 0;

            StartCoroutine(SpawnWave(rounds[currentRound].waves[currentWave]));
        }
    }

    void FinishedKillingWaves()
    {
        if (currentRound < rounds.Length - 1)
        {
            lastRound = false;

            currentWave = 0;

            currentRound++;
            amountSpawned = 0;

            StartCoroutine(StartRound(rounds[currentRound]));

            return;
        }
        else
        {
            lastRound = false;

            currentRound = 0;

            FinishedGame();
        }
    }

    public void CheckEnemyCount(SpriteRenderer sr)
    {
        
    }

    public virtual void FinishedGame()
    {
        StopAllCoroutines();

        enemies.Clear();

        GetComponent<IEvent>().FinishedGame();
    }
}

[System.Serializable]
public class Wave
{
    public string enemyTag;
    public int amountToSpawn;
    public Transform spawnPoint;
}