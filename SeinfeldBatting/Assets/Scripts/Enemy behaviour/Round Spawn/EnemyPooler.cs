﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPooler : MonoBehaviour
{
    [System.Serializable]
    public class Pool
    {
        public string tag;
        public GameObject prefab;
        public int size;
    }

    #region Singleton
    public static EnemyPooler instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion

    public List<Pool> pools;
    public Dictionary<string, Queue<GameObject>> enemyPool;

    public PerformanceObject po;

    // Start is called before the first frame update
    void Start()
    {
        enemyPool = new Dictionary<string, Queue<GameObject>>();

        foreach(Pool pool in pools)
        {
            Queue<GameObject> objectPool = new Queue<GameObject>();

            for (int i = 0; i < pool.size; i++)
            {
                GameObject obj = Instantiate(pool.prefab);
                obj.SetActive(false);
                objectPool.Enqueue(obj);
            }

            enemyPool.Add(pool.tag, objectPool);
        }
    }

    public GameObject SpawnFromPool(string tag, Vector2 position, Quaternion rotation)
    {
        if(!enemyPool.ContainsKey(tag))
        {
            Debug.LogWarning("Key with tag " + tag + " does not exist");
            return null;
        }

        GameObject objToSpawn = enemyPool[tag].Dequeue();

        objToSpawn.SetActive(true);
        objToSpawn.transform.position = position;
        objToSpawn.transform.rotation = rotation;
        objToSpawn.GetComponent<Enemy>().po = po;

        objToSpawn.GetComponent<IPooledEnemy>().OnObjectRePool();
        
        enemyPool[tag].Enqueue(objToSpawn);

        return objToSpawn;
    }
}
