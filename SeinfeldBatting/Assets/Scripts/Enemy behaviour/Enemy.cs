﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [Header("Enemy Variables")]
    public float damage;
    public int pointsToAward;
    public ParticleSystem deathEffect;
    public EnemyHealthController ehc;

    [HideInInspector] public PerformanceObject po;
    [HideInInspector] public ShipController sc;
    [HideInInspector] public EnemySpawner father;

    // Start is called before the first frame update
    public virtual void Start()
    {
        if(ehc == null)
            ehc = GetComponent<EnemyHealthController>();

        if(ehc != null)
            ehc.OnDeath += Death;
    }

    public delegate void EnemyDeath(SpriteRenderer enemy);
    public event EnemyDeath OnEnemyDeath;

    public virtual void Death()
    {
        if(deathEffect != null)
        {
            Instantiate(deathEffect, transform.position, Quaternion.identity);
        }

        ScoreManager.AddScore(pointsToAward);
        ScoreManager.instance.amountKilled++;

        if(father != null)
        {
            father.enemies.Remove(gameObject);
        }

        OnEnemyDeath?.Invoke(GetComponent<SpriteRenderer>());

        gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        sc = collision.GetComponent<ShipController>();

        if(sc != null)
        {
            OnPlayerHit();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        // THIS MAY FUCK SHIT UP IN THE FUTURE, DUMBASS

        if(sc != null)
        {
            sc = null;
        }
    }

    public virtual void OnPlayerHit()
    {

    }
}
