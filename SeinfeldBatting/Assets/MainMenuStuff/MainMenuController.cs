﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Audio;

public class MainMenuController : MonoBehaviour
{
    public string startSceneName;
	
	public Image background;
    public Slider slider;
    public TMP_Text text;
	public TMP_Dropdown resolutionDrop;
	
	public Sprite[] backgrounds;
	
	public AudioMixer audioMixer;
	
	Resolution[] resolutions;
	
	public void Start()
	{
		RandomBG();
		
		resolutions = Screen.resolutions;
		
		resolutionDrop.ClearOptions();
		
		List<string> options = new List<string>();
		
		int currentIndex = 0;
		for	(int i = 0; i < resolutions.Length; i++)
		{
			string option = resolutions[i].width + " x " + resolutions[i].height;
			options.Add(option);
			
			if(resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height)
			{
				currentIndex = i;
			}
		}
		
		resolutionDrop.AddOptions(options);
		resolutionDrop.value = currentIndex;
		resolutionDrop.RefreshShownValue();
	}
	
    public void StartGame()
    {
        StartCoroutine(LoadAsync(startSceneName));
    }

    IEnumerator LoadAsync(string sceneName)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(startSceneName);

        while(!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / 0.9f);

            slider.value = progress;

            text.text = progress * 100 + "%";

            yield return null;
        }
    }
	
	public void SetVolume(float volume)
	{
		audioMixer.SetFloat("Volume", volume);
	}
	
	public void SetQuality(int index)
	{
		QualitySettings.SetQualityLevel(index);
	}
	
	public void SetFullscreen(bool isTrue)
	{
		Screen.fullScreen = isTrue;
	}
	
	public void SetResolution(int rezIndex)
	{
		Resolution rez = resolutions[rezIndex];
		Screen.SetResolution(rez.width, rez.height, Screen.fullScreen);
	}

    public void Exit()
    {
        Application.Quit();
    }
	
	[ContextMenu("Change BG")]
	void RandomBG()
	{
		//Set random background
		
		int random = Random.Range(0, backgrounds.Length - 1);
		
		background.sprite = backgrounds[random];
	}
}
